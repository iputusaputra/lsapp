@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-success">Go Back</a>
    <h1>{{$post->title}}</h1>
    <img style="width:100%" src="/storage/cover_images/{{$post->cover_image}}">
    <br><br>
    <div>
        {!!$post->body!!}
    </div>
    <hr>
    <small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
    <hr>
    <div class="row">
    @if (!Auth::guest())
        @if (Auth::user()->id == $post->user_id)
        <div class="col-md-11">
            <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a>
        </div>

        <div class="col-md-1">
        {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
        {!!Form::close()!!}
        </div>
        @endif
    @endif
    </div>

    @endsection