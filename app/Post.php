<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //you can change table attribute here

    // Table name
    //protected $table = 'mypost';
    protected $table = 'posts';
    // Primary key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    //add relationship
    public function user() {
        return $this->belongsTo('App\User');
    }
}
